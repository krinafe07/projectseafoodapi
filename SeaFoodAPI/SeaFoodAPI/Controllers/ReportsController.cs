﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeaFoodAPI.Service.Services;

namespace SeaFoodAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Reports")]

    public class ReportsController : ControllerBase
    {
        private readonly IReportService _reportService;
        public ReportsController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]

        public IActionResult GetReportUsers()
        {
            try
            {
                var fileContents = _reportService.GetReportUsers();
                return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "ReporteUsuarios.xlsx"
            );
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
       

    }
}
