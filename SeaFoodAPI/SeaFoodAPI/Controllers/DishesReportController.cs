﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeaFoodAPI.Service.Services;

namespace SeaFoodAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/DishReports")]
    public class DishesReportController : ControllerBase
    {
        private readonly IDishReportService _dishReportService;
        public DishesReportController(IDishReportService dishreportService)
        {
            _dishReportService = dishreportService;
        }
        [HttpGet]

        public IActionResult GetReporDishes()
        {
            try
            {
                var fileContents = _dishReportService.GetReportDishes();
                return File(
                fileContents: fileContents.Result,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "ReporteUsuarios.xlsx"
            );
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
