﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeaFoodAPI.Model.Model;
using SeaFoodAPI.Repository.Data;
using SeaFoodAPI.Repository.Repositories;
using SeaFoodAPI.Service.Services;

namespace SeaFoodAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
      //  private readonly DataDbContext _context;
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            this._userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult GetAllUser()
        {
            try
            {
                var Users = _userService.GetUsers();
                return Ok(Users);
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }
        // GET: api/User1/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var userFinded = _userService.GetId(id);

                return Ok(userFinded);
            }
            catch (Exception)
            {

                throw;
            }
        }
        // PUT: api/User1/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser( int id, [FromBody] User user)
        {

            try
            {
                _userService.Update(id, user);
                var email = "krinafe07@gmail.com";
                var smtpClient = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(email, "lafonthaine050419")
                };
                var message = new MailMessage(email, user.Email)
                {
                    Subject = "Actualización de usuario",
                    Body = $"Hola {user.Name} " + "\r\n" +
                    $"¡El usuario resgistrado en Seafood Los Gordos se ha actualizado correctamente!"
                };
                using (message)
                    smtpClient.Send(message);
                return Ok("se actualizo el usuario sactisfactoriamente");

               
            }
            catch (Exception)
            {

                return BadRequest();
            }

        }

        // POST: api/User1
        [HttpPost]
        public void PostUser([FromBody] User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userService.AddUser(user);
                }


            }
            catch (Exception)
            {

            }


        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

            try
            {
                _userService.Delete(id);

            }
            catch (Exception)
            {

                BadRequest();
            }

        }
    }
}