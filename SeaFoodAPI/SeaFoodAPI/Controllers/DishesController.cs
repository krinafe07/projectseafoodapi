﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeaFoodAPI.Model.Models;
using SeaFoodAPI.Repository.Data;
using SeaFoodAPI.Service.Services;

namespace SeaFoodAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DishesController : ControllerBase
    {
        private readonly IDishService _dishService;
        public DishesController(IDishService dishService)
        {
            this._dishService = dishService;
        }

        // GET: api/Dishes
        [HttpGet]
        public async Task<IActionResult> GetDish()
        {
            try
            {
                return Ok(await _dishService.GetDishes());
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        // GET: api/Dishes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDish([FromRoute] int id)
        {
            try
            {
                return Ok(await _dishService.GetId(id));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        // PUT: api/Dishes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDish([FromRoute] int id, [FromBody] Dish dish)
        {
            try
            {
                return Ok(await _dishService.update(id, dish));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        // POST: api/Dishes
        [HttpPost]
        public async Task<IActionResult> PostDish([FromBody] Dish dish)
        {
            try
            {
                return Ok(await _dishService.Add(dish));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Dishes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDish([FromRoute] int id)
        {
            try
            {
                return Ok(await _dishService.Delete(id));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}