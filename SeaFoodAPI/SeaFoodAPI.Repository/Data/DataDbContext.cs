﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SeaFoodAPI.Model.Model;
using SeaFoodAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeaFoodAPI.Repository.Data
{
    public class DataDbContext : IdentityDbContext<ApplicationUser>
    {
        public DataDbContext()
        {

        }

        public DataDbContext(DbContextOptions options)
        : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //En caso de que el contexto no este configurado, lo configuramos mediante la cadena de conexion
            if (!optionsBuilder.IsConfigured)
            {
                //Server=.;Database=SeaFood;Integrated Security=True;
                //Server = .;Database=Seafood; Uid=sa; Pwd=uts;
                optionsBuilder.UseSqlServer("Server = 172.16.28.244;Database=Seafood; Uid=sa; Pwd=uts;");
            }
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Dish> Dish { get; set; }
    }
}
