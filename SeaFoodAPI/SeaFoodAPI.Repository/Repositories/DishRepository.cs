﻿using Microsoft.EntityFrameworkCore;
using SeaFoodAPI.Model.Models;
using SeaFoodAPI.Repository.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Repository.Repositories
{
    public interface IDishRepository
    {
        Task<IEnumerable<Dish>> GetDishes();
        Task<Dish> Delete(int id);
        Task<Dish> GetId(int id);
        Task<Dish> Add(Dish dish);
        Task<Dish> Update(int id, Dish dish);
    }

    public class DishRepository : IDishRepository
    {
        private readonly DataDbContext _dbContext = new DataDbContext();
        public async Task<Dish> GetId(int id)
        {
            {
                //return _dbContext.User.Find(id);
                var dish = await _dbContext.Dish.FindAsync(id);
                return (dish);
            }

        }

        public async Task<IEnumerable<Dish>> GetDishes()
        {
            var dish = await _dbContext.Dish.ToArrayAsync();
            return (dish);

        }

        public async Task<Dish> Add(Dish dish)
        {
            _dbContext.Dish.Add(dish);
            await _dbContext.SaveChangesAsync();
            return dish;

        }
        public async Task<Dish> Update(int id, Dish dish)
        {
            _dbContext.Entry(dish).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return dish;
        }
        public async Task<Dish> Delete(int id)
        {
            var dish = await _dbContext.Dish.FindAsync(id);
            _dbContext.Dish.Remove(dish);
            await _dbContext.SaveChangesAsync();
            return dish;
        }
    }
}
