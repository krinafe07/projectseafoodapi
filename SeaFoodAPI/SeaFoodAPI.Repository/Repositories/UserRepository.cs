﻿using Microsoft.EntityFrameworkCore;
using SeaFoodAPI.Model.Model;
using SeaFoodAPI.Repository.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Repository.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User GetId(int id);
        Task<User> Insert(User user);
        void Delete(int id);
        User Update(int id, User user);

    }
    public class UserRepository : IUserRepository
    {
        private readonly DataDbContext _dbContext = new DataDbContext();
        public void Delete(int id)
        {
            _dbContext.Remove(GetId(id));
            _dbContext.SaveChanges();
        }

        public User GetId(int id)
        {
            {
                return _dbContext.User.Find(id);
            }

        }

        public IEnumerable<User> GetAll()
        {

            return _dbContext.User;
        }

        public async Task<User> Insert(User user)
        {
            _dbContext.User.Add(user);
            await _dbContext.SaveChangesAsync();
            return user;
        }

        public User Update(int id, User user)
        {

            var userNew = GetId(user.Id);
            userNew.Name = user.Name;
            userNew.Lastname= user.Lastname;
            userNew.Phone= user.Phone;
            userNew.Email = user.Email;
            userNew.Password = user.Password;
            userNew.Age= user.Age;
            _dbContext.Update(userNew);
            _dbContext.SaveChanges();
            return user;
        }

    }
}
