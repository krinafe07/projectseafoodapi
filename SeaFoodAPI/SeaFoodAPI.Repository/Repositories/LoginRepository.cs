﻿using SeaFoodAPI.Model.Model;
using SeaFoodAPI.Model.Models;
using SeaFoodAPI.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeaFoodAPI.Repository.Repositories
{
    public interface ILoginRepository
    {
        User getUser(Login loging);
    }
    public class LoginRepository: ILoginRepository
    {
        private readonly DataDbContext _db = new DataDbContext();
        public User getUser(Login loging)
        {
            return _db.User.Where(x => x.Email == loging.Email && x.Password == loging.Password).FirstOrDefault();
        }
    }
}
