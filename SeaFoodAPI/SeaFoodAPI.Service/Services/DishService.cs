﻿using SeaFoodAPI.Model.Models;
using SeaFoodAPI.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Service.Services
{
    public interface IDishService
    {
        Task<IEnumerable<Dish>> GetDishes();
        Task<Dish> Delete(int id);
        Task<Dish> GetId(int id);
        Task<Dish> Add(Dish dish);
        Task<Dish> update(int id, Dish dish);

    }
    public class DishService : IDishService
    {
        private readonly IDishRepository _dishRepository;
        public DishService(IDishRepository dishRepository)
        {
            _dishRepository = dishRepository;
        }
        public async Task<IEnumerable<Dish>> GetDishes()
        {
            var dish = await _dishRepository.GetDishes();
            return (dish);
        }

        public async Task<Dish> GetId(int id)
        {
            var dish = await _dishRepository.GetId(id);
            return (dish);
        }

        public async Task<Dish> Add(Dish dish)
        {
            await _dishRepository.Add(dish);
            return dish;
        }
        public async Task<Dish> Delete(int id)
        {
            var user = await _dishRepository.Delete(id);
            return user;
        }
        public async Task<Dish> update(int id, Dish dish)
        {
            await _dishRepository.Update(id, dish);
            return dish;
        }
    }
}
