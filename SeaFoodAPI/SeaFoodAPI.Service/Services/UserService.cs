﻿using SeaFoodAPI.Model.Model;
using SeaFoodAPI.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Service.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        void Delete(int id);
        User GetId(int id);
        Task<User> AddUser(User user);
        User Update(int id, User user);

    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public IEnumerable<User> GetUsers()
        {

            return _userRepository.GetAll();
        }
        public void Delete(int id)
        {
            _userRepository.Delete(id);
        }

        public User GetId(int id)
        {
            return _userRepository.GetId(id);
        }

        public async Task<User> AddUser(User user)
        {
            await   _userRepository.Insert(user);

            var email = "krinafe07@gmail.com";
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential(email, "lafonthaine050419")
            };
            var message = new MailMessage(email, user.Email)
            {
                Subject = "¡Bienvenido a Seafood Los Gordos!",
                Body = $"Hola {user.Name} " + "\r\n" +
                $"¡Te haz registrado exitosamente a Mariscos Los Gordos, te invitamos a probar nuestros deliciosos platillos, BIENVENIDO!"
            };
            using (message)
                smtpClient.Send(message);

            return user;
        }

        public void SendEmail()
        {
            //var email = "krinafe07@gmail.com";
            //var smtpClient = new SmtpClient
            //{
            //    Host = "smtp.gmail.com",
            //    Port = 587,
            //    EnableSsl = true,
            //    Credentials = new NetworkCredential(email, "lafonthaine050419")
            //};
            //var message = new MailMessage(email, user.Email)
            //{
            //    Subject = "¡Bienvenido a Seafood Los Gordos!",
            //    Body = $"Hola {user.Name} " + "\r\n" +
            //    $"¡Te haz registrado exitosamente a Mariscos Los Gordos, te invitamos a probar nuestros deliciosos platillos, BIENVENIDO!"
            //};
            //using (message)
            //    smtpClient.Send(message);
        }

        public User Update(int id, User user)
        {
           return _userRepository.Update(id, user);
        }

    }
}
