﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using SeaFoodAPI.Model.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Service.Services
{
    public interface IReportService
    {
       byte[] GetReportUsers();
    }
    public class ReportService : IReportService
    {
        private readonly IUserService _userService;
        public ReportService(IUserService userService)
        {
            _userService = userService;
        }
        public  byte[] GetReportUsers()
        {
            byte[] fileContents;

            //DATOS DEL REPORTE
            IEnumerable<User> usersData =  _userService.GetUsers();
            //DATOS DEL REPORTE 

            using (var package = new ExcelPackage())
            {
                int row = 1;
                //AGREGA HOJA NUEVA
                var worksheet = package.Workbook.Worksheets.Add("ReportUsers");
                //COLUMNA 1 NOMBRE
                CrearHeader(row, 1, "Nombre", worksheet);
                CrearHeader(row, 2, "Apellido", worksheet);
                CrearHeader(row, 3, "Telefono", worksheet);
                CrearHeader(row, 4, "Edad", worksheet);
                CrearHeader(row, 5, "Email", worksheet);



                foreach (User _user in usersData)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = _user.Name.ToUpper();
                    worksheet.Cells[row, 2].Value = _user.Lastname.ToUpper();
                    worksheet.Cells[row, 3].Value = _user.Phone.ToUpper();
                    worksheet.Cells[row, 4].Value = _user.Age.ToString().ToUpper();
                    worksheet.Cells[row, 5].Value = _user.Email.ToUpper();
                }

                fileContents = package.GetAsByteArray();

                return fileContents;
            }

        }

        private void CrearHeader(int indexRow, int indexCol, string NombreColumna, ExcelWorksheet excelWorksheet)
        {
            excelWorksheet.Cells[indexRow, indexCol].Value = NombreColumna;
            excelWorksheet.Cells[indexRow, indexCol].Style.Font.Size = 12;
            excelWorksheet.Cells[indexRow, indexCol].Style.Font.Bold = true;
            excelWorksheet.Cells[indexRow, indexCol].Style.Border.Top.Style = ExcelBorderStyle.Hair;
            excelWorksheet.Cells[indexRow, indexCol].AutoFitColumns();
            excelWorksheet.Cells[indexRow, indexCol].AutoFilter = true;
            //Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7DEE8");
            //excelWorksheet.Cells[indexRow, indexCol].Style.Fill.BackgroundColor.SetColor(colFromHex);

        }

    }
}