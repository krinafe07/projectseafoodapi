﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using SeaFoodAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodAPI.Service.Services
{
    public interface IDishReportService
    {
        Task<byte[]> GetReportDishes();
    }
    public class DishReportService : IDishReportService
    {
        private readonly IDishService _dishService;
        public DishReportService(IDishService dishService)
        {
            _dishService = dishService;
        }
        public async Task<byte[]>  GetReportDishes()
        {
            byte[] fileContents;

            //DATOS DEL REPORTE
            IEnumerable<Dish> dishesData = await _dishService.GetDishes();
            //DATOS DEL REPORTE 

            using (var package = new ExcelPackage())
            {
                int row = 1;
                //AGREGA HOJA NUEVA
                var worksheet = package.Workbook.Worksheets.Add("ReportUsers");
                //COLUMNA 1 NOMBRE
                CrearHeader(row, 1, "Tipo de platillo", worksheet);
                CrearHeader(row, 2, "Nombre", worksheet);
                CrearHeader(row, 3, "Precio", worksheet);
                CrearHeader(row, 4, "Descripción", worksheet);
                



                foreach (Dish _dish in dishesData)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = _dish.TypeDish.ToUpper();
                    worksheet.Cells[row, 2].Value = _dish.Name.ToUpper();
                    worksheet.Cells[row, 3].Value = _dish.Price.ToString().ToUpper();
                    worksheet.Cells[row, 4].Value = _dish.Description.ToUpper();
                }

                fileContents = package.GetAsByteArray();

                return fileContents;
            }

        }

        private void CrearHeader(int indexRow, int indexCol, string NombreColumna, ExcelWorksheet excelWorksheet)
        {
            excelWorksheet.Cells[indexRow, indexCol].Value = NombreColumna;
            excelWorksheet.Cells[indexRow, indexCol].Style.Font.Size = 12;
            excelWorksheet.Cells[indexRow, indexCol].Style.Font.Bold = true;
            excelWorksheet.Cells[indexRow, indexCol].Style.Border.Top.Style = ExcelBorderStyle.Hair;
            excelWorksheet.Cells[indexRow, indexCol].AutoFitColumns();
            excelWorksheet.Cells[indexRow, indexCol].AutoFilter = true;
            //Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7DEE8");
            //excelWorksheet.Cells[indexRow, indexCol].Style.Fill.BackgroundColor.SetColor(colFromHex);

        }

    }
}
