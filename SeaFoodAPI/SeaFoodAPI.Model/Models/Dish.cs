﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaFoodAPI.Model.Models
{
    public class Dish : Entity
    {
        public string TypeDish { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}
