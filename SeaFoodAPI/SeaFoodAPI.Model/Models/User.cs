﻿using SeaFoodAPI.Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SeaFoodAPI.Model.Model
{
    public class User : Entity
    {
        //Nombre del usuario
        [MaxLength(100), Required(ErrorMessage = "Se necesita capturar tu nombre"), RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Usa solamente letras")]
        public string Name { get; set; }

        //Apellido del usuario
        [MaxLength(100), Required(ErrorMessage = "Se necesita capturar tu apellido"), RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Usa solamente letras")]
        public string Lastname { get; set; }

        //Celular del usuario
        [Required(ErrorMessage = "Se requiere un numero de telefono")]
        [MaxLength(20)]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Por favor ingrese un numero valido")]
        public string Phone { get; set; }

        ////Correo del usuario
        [Required(ErrorMessage = "Se requiere un correo electronico")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "el correo es invalido")]
        [MaxLength(50)]
        public string Email { get; set; }

        //Contraseña del usuario
        [MaxLength(10), Required(ErrorMessage = "Se necesita capturar una contraseña"), MinLength(4)]
        public string Password { get; set; }


        ////Edad del usuario
        [Range(18, 56, ErrorMessage = "Debe contar con la mayoria de edad")]
        public int Age { get; set; }

        //TipodeUsuario
        public string Rol { get; set; }
    }
}
