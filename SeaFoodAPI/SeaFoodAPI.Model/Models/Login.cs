﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaFoodAPI.Model.Models
{
    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
